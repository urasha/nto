using UnityEngine;
using UnityEngine.Events;

public class CurseDistanceChecker : MonoBehaviour
{
    [SerializeField] private GameObject _player;

    public UnityAction CurseActivated;
    public UnityAction CurseReset;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == _player)
        {
            CurseActivated?.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == _player)
        {
            CurseReset?.Invoke();
        }
    }

}
