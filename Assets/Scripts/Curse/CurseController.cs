using UnityEngine;
using System.Collections;
using System;
using TMPro;

public class CurseController : MonoBehaviour
{
    private const float StackPeriod = 1.5f;

    [SerializeField] private PurificationController _pureController;
    [SerializeField] private GameObject _curseStackText;
    [SerializeField] private CurseDistanceChecker _checker;

    private TextMeshProUGUI _text;
    private IEnumerator _curseStacksCoroutine;
    private float _stacks = 0f;
    public float Stacks { 
        get => _stacks; 
        set => _stacks = value; 
    }

    private void Start()
    {
        _checker.CurseActivated += StartNegativeEffect;
        _checker.CurseReset += StopStacking;

        _text = _curseStackText.GetComponent<TextMeshProUGUI>();
    }

    private void StartNegativeEffect()
    {
        _curseStackText.SetActive(true);

        _curseStacksCoroutine = StackBebuff();
        StartCoroutine(_curseStacksCoroutine);
    }

    private void StopStacking()
    {
        StopCoroutine(_curseStacksCoroutine);
    }

    private void ResetStacks()
    {
        _stacks = 0;
        _text.text = "0";
        _curseStackText.SetActive(false);
    }

    private IEnumerator StackBebuff()
    {
        while (true)
        {
            yield return new WaitForSeconds(StackPeriod);
            _stacks++;
            _text.text = Convert.ToString(_stacks);
        }
    }
}
