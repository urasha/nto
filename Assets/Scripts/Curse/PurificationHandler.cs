using UnityEngine;

public class PurificationHandler : MonoBehaviour
{
    private const float PurificationY = -4.25f;

    [SerializeField] private float _minSpawnDistance;
    [SerializeField] private float _maxSpawnDistance;
    [SerializeField] private CurseDistanceChecker _checker;
    [SerializeField] private GameObject _purification;
    [SerializeField] private Transform _curse;

    private bool _isCreated = false;

    private void Start()
    {
        _checker.CurseActivated += CreatePurification;
    }

    private void CreatePurification()
    {   
        if (!_isCreated)
        {
            float x = Random.Range(_minSpawnDistance, _maxSpawnDistance);
            Vector2 position = new Vector2(x + _curse.transform.position.x, PurificationY);

            _purification.transform.position = position;

            _isCreated = true;
        }
    }
}
