using UnityEngine;

public class CurseObjectMovement : MonoBehaviour
{   
    // TODO: change parametrs for last scene
    private const float MinY = -2.15f;
    private const float MaxY = 6.45f;
    private const float MaxPathTime = 1.8f;

    [SerializeField] private PurificationController _pureController;
    [SerializeField] private CurseDistanceChecker _checker;
    [SerializeField] private Transform _playerPosition;
    [SerializeField] private float _radiusX;
    [SerializeField] private float _radiusY;
    [SerializeField] private float _defaultSpeed;
    [SerializeField] private float _teleportationDistance;
    [SerializeField] private float _teleportationCooldown;

    private bool _isMoving = false;
    private float _pathTimer = 0f;
    private float _teleportationTimer = 0f;
    private Vector2 _generatedPoint;

    private void Start()
    {
        _checker.CurseActivated += StartMoving;
        _pureController.PurificationActivated += StopMoving;

        _teleportationTimer = _teleportationCooldown;
    }

    private void Update()
    {   
        if (_isMoving)
        {
            float playerX = _playerPosition.transform.position.x;
            float playerY = _playerPosition.transform.position.y;
            float curseX = transform.position.x;
            float curseY = transform.position.y;

            if (Mathf.Sqrt(Mathf.Pow(playerX -curseX, 2) + Mathf.Pow(playerY - curseY, 2)) <= _teleportationDistance)
                Teleport();

            if (_pathTimer >= MaxPathTime)
            {
                _generatedPoint = GeneratePathPoint();
                _pathTimer = 0f;
            }

            Move();
        }
    }

    private void StartMoving()
    {
        _isMoving = true;
        _generatedPoint = GeneratePathPoint();
    }

    private void StopMoving()
    {
        _isMoving = false;
    }

    private void Teleport()
    {   
        if (Time.time >= _teleportationTimer)
        {
            float x = Random.Range(_radiusX * 0.65f, _radiusX * 0.8f);
            float y = Random.Range(-_radiusY * 0.45f, _radiusY * 0.45f);

            if (_playerPosition.transform.position.x > transform.position.x)
                x = -x;

            transform.position = new Vector2(transform.position.x + x, transform.position.y + y);

            _teleportationTimer = Time.time + _teleportationCooldown;
        }
    }   

    private void Move()
    {
        float speed = _defaultSpeed * Time.deltaTime;

        float xProgress = Mathf.Lerp(transform.position.x, _generatedPoint.x, speed);
        float yProgress = Mathf.Lerp(transform.position.y, _generatedPoint.y, speed);
        transform.position = new Vector2(xProgress, yProgress);

        _pathTimer += Time.deltaTime;
    }

    private Vector2 GeneratePathPoint()
    {
        float x = Random.Range(-_radiusX, _radiusX);
        float y = Random.Range(-_radiusY, _radiusY);

        if (transform.position.y >= MaxY)
            y *= -1.3f; // fast fly down
        if (transform.position.y <= MinY)
            y *= 1.3f; // fast fly up

        Vector2 endPoint = new Vector2(transform.position.x + x, transform.position.y + y);
        
        return endPoint;
    }
}
