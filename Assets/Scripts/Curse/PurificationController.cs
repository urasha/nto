using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class PurificationController : MonoBehaviour
{
    public UnityAction PurificationActivated;

    [SerializeField] private float _waitingTime;
    [SerializeField] private CurseController _curseController;
    [SerializeField] private GameObject _curseStackText;
    [SerializeField] private PlayerHealth _healthScript;
    [SerializeField] private Image _heart;

    private Animator _animator;
    private Coroutine _activator;
    private TextMeshProUGUI _text;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _text = _curseStackText.GetComponent<TextMeshProUGUI>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            _activator = StartCoroutine(ActivatePurification());
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            StopCoroutine(_activator);
    }

    private IEnumerator ActivatePurification()
    {
        yield return new WaitForSeconds(_waitingTime);

        #region Stacks controll
        _curseController.Stacks = 0;
        _text.text = "0";
        _curseStackText.SetActive(false);
        #endregion

        #region Health restore
        _healthScript.Health += Mathf.Clamp((int)Mathf.Ceil(_healthScript._takenCurseDamage / 2), 0, 100);
        _healthScript._takenCurseDamage = 0;

        _heart.color = new Color(0.88f, 0.54f, 0.54f);
        #endregion

        _animator.Play("destroy");

        GameObject[] curses = GameObject.FindGameObjectsWithTag("Curse");

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        GameObject closest = new();
        foreach (GameObject go in curses)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }


        Destroy(closest);
        Destroy(gameObject, 1.5f);
    }
}
