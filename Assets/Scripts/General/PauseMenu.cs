using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool IsPaused = false;

    [SerializeField] private GameObject _menu;

    private void Start()
    {
        InputHandler.Instance.PausePressed += ChangeGameState;
    }

    public void ChangeGameState()
    {
        if (IsPaused)
            Resume();
        else
            Pause();
    }

    private void Resume()
    {
        Debug.Log("resume");
        _menu.SetActive(false);
        Time.timeScale = 1f;
        IsPaused = false;
    }

    private void Pause()
    {
        Debug.Log("pause");
        _menu.SetActive(true);
        Time.timeScale = 0f;
        IsPaused = true;
    }
}
