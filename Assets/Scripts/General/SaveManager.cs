using System.Collections.Generic;
using UnityEngine;

public static class SaveManager
{
    public static void Save<T>(string key, T data)
    {
        string jsonData = JsonUtility.ToJson(data, true);
        PlayerPrefs.SetString(key, jsonData);
    }

    public static T Load<T>(string key) where T: new()
    {
        if (PlayerPrefs.HasKey(key))
        {
            string loadedData = PlayerPrefs.GetString(key);
            return JsonUtility.FromJson<T>(loadedData);
        }
        else
        {
            return new T();
        }
    }
}
