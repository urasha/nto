public struct StatesAnimation
{
    public static readonly string IsRunning = "isRunning";
    public static readonly string PressedAttack = "PressedAttack";
    public static readonly string AttackName = "Attack";
    public static readonly string DeathName = "Death";
}
