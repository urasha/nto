using UnityEngine;

namespace SaveData
{
    [System.Serializable]
    public class PlayerProfiler : MonoBehaviour
    {
        public Vector2 SavePosition;

        public PlayerProfiler()
        {
            SavePosition = new Vector2(-5.15f, 1.2f); 
        }
    }
}

