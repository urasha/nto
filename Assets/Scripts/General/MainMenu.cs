using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Animator _transitionAnimator;
    [SerializeField] private float _transitionTime;

    public void LoadGame(int index)
    {
        StartCoroutine(LoadLevel(index));
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public IEnumerator LoadLevel(int levelIndex)
    {
        _transitionAnimator.SetTrigger("Start");
        Time.timeScale = 1f;

        yield return new WaitForSeconds(_transitionTime);

        SceneManager.LoadScene(levelIndex);
    }
}
