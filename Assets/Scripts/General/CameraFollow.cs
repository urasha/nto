using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private float _followSpeed;
    [SerializeField] private float _offsetY;
    [SerializeField] private Transform _target;

    private void Update()
    {
        Vector3 newPosition = new Vector3(_target.position.x, _target.position.y + _offsetY, -10f);
        transform.position = Vector3.Slerp(transform.position, newPosition, _followSpeed);
    }
}
