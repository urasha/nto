using UnityEngine;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour
{   
    public static InputHandler Instance { get; private set; }

    public UnityAction AttackPressed;
    public UnityAction RollPressed;
    public UnityAction InteractPressed;
    public UnityAction PausePressed;

    private Vector2 _movementInput;
    public Vector2 MovementInput { 
        get => _movementInput; 
        set => _movementInput = value; 
    }

    private InputMaster _inputActions;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void OnEnable()
    {
        if (_inputActions == null)
        {
            _inputActions = new InputMaster();

            _inputActions.Player.Movement.performed += input => _movementInput = input.ReadValue<Vector2>();
            _inputActions.Player.Movement.canceled += _ => _movementInput = new Vector2(0, _movementInput.y);

            _inputActions.Player.Attack.performed += _ => AttackPressed?.Invoke();

            _inputActions.Player.Roll.performed += _ => RollPressed?.Invoke();

            _inputActions.Player.Interact.performed += _ => InteractPressed?.Invoke();

            _inputActions.Player.Pause.performed += _ => PausePressed?.Invoke();
        }

        _inputActions.Enable();
    }

    private void OnDisable()
    {
        _inputActions.Disable();
    }
}
