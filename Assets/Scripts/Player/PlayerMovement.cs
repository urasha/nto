using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    [SerializeField] private float _speed = 6.25f;

    private SpriteRenderer _sprite;
    private Rigidbody2D _rigidBody;
    private bool _isFacingRight = true;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _sprite = GetComponent<SpriteRenderer>();

    }

    private void Update()
    {   
        if ((InputHandler.Instance.MovementInput.x > 0 && !_isFacingRight) || (InputHandler.Instance.MovementInput.x < 0 && _isFacingRight))
            Flip();
    }

    private void FixedUpdate()
    {
        if(AnimationHandler.Animator.GetCurrentAnimatorStateInfo(0).IsName(StatesAnimation.AttackName))
        {
            _rigidBody.velocity = Vector2.zero;
            return;
        }

        if (AnimationHandler.Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name != "Roll")
        {
            _rigidBody.velocity = new Vector2(InputHandler.Instance.MovementInput.x * _speed, _rigidBody.velocity.y);
            AnimationHandler.Animator.SetBool(StatesAnimation.IsRunning, _rigidBody.velocity.x != 0);
        }
    }

    private void PlayStepSound()
    {
        _audio.pitch = Random.Range(0.95f, 1.1f);
        _audio.Play();
    }

    private void Flip()
    {
        Vector3 currentScale = transform.localScale;
        currentScale.x *= -1;
        transform.localScale = currentScale;

        _isFacingRight = !_isFacingRight;

        _audio.Stop();
    }
} 
