using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerRoll : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private List<string> _posibleAnimationsForRoll;

    [Header("Stats")]
    [SerializeField] private float _dashPower;
    [SerializeField] private float _rollCooldown;

    private Rigidbody2D _rigidBody;
    private PlayerStamina _staminaScript;

    private float _nextRollTime = 0f;

    private void Start()
    {
        InputHandler.Instance.RollPressed += Roll;

        _staminaScript = GetComponent<PlayerStamina>();
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    private void Roll()
    {
        if (CheckRollPosible() && Time.time >= _nextRollTime && _staminaScript.Stamina >= _staminaScript.RequiredValue)
        {
            AnimationHandler.Animator.SetTrigger("RollPressed");

            _nextRollTime = Time.time + _rollCooldown;
            
            _staminaScript.ExpendStamina?.Invoke();

            StartCoroutine(MoveRoll());
        }
    }

    private bool CheckRollPosible()
    {
        return _posibleAnimationsForRoll.Contains(AnimationHandler.Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name);
    }

    private IEnumerator MoveRoll()
    {
        _rigidBody.velocity = new Vector2(-transform.localScale.x * _dashPower, 0f);

        yield return null;
    }
}
