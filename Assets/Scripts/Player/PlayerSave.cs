using UnityEngine;

public class PlayerSave : MonoBehaviour
{
    private const string SaveKey = "MainSave";

    private readonly float _distance = 0.8f;

    private Vector2 _currentSavePosition;

    private void Start()
    {
        InputHandler.Instance.InteractPressed += TryToSave;
    }

    private void TryToSave()
    {
        #region Find Closest Save
        GameObject[] curses = GameObject.FindGameObjectsWithTag("Curse");

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        GameObject closest = new();
        foreach (GameObject go in curses)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        #endregion

        // Check save's position
        if (Mathf.Abs(closest.transform.position.x - transform.position.x) <= _distance)
        {
            _currentSavePosition = closest.transform.position;
            Save();
        }
    }

    private void Save()
    {
        SaveManager.Save(SaveKey, GetSaveSnapshot());
    }

    private void Load()
    {
        var data = SaveManager.Load<SaveData.PlayerProfiler>(SaveKey);
        
        // use data
    }

    private SaveData.PlayerProfiler GetSaveSnapshot()
    {
        var data = new SaveData.PlayerProfiler()
        {
            SavePosition = _currentSavePosition,
        };

        return data;
    }


}
