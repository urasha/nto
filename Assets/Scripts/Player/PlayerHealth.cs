using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class PlayerHealth : MonoBehaviour
{
    private const int MaxHealth = 100;
    private const float DefaultCurseDamage = 0.5f;
    private const float CurseDamagePeriod = 0.7f;
    
    private readonly Color _curseHeartColor = new Color(0.34f, 0.16f, 0.16f);
    private readonly Color _defaultHeartColor = new Color(0.88f, 0.54f, 0.54f);

    public static UnityAction Death;

    [SerializeField] private PurificationController _pureController;
    [SerializeField] private CurseDistanceChecker _checker;
    [SerializeField] private CurseController _curseController;
    [SerializeField] private Image _healthBar;
    [SerializeField] private Image _heart;

    private IEnumerator _curseDamageCoroutine;

    private float _lerpSpeed = 3f;
    public int _takenCurseDamage = 0;
    private int _health = 100;
    public int Health { 
        get => _health; 
        set => _health = value; 
    }

    private void Start()
    {
        _checker.CurseActivated += StartCurseDamage;
        _checker.CurseReset += StopCurseDamage;
    }

    private void Update()
    {
        _lerpSpeed = 3f * Time.deltaTime;
        FillHealthBar();
    }

    private void FillHealthBar()
    {
        _healthBar.fillAmount = Mathf.Lerp(_healthBar.fillAmount, (float)_health / (float)MaxHealth, _lerpSpeed);
    }

    public void TakeDamage(int damage)
    {
        _health -= damage;

        if (_health <= 0)
            Death?.Invoke();
    }

    private void StartCurseDamage()
    {
        _heart.color = _curseHeartColor;

        _curseDamageCoroutine = TakeCurseDamage();
        StartCoroutine(_curseDamageCoroutine);
    }

    private void StopCurseDamage()
    {
        StopCoroutine(_curseDamageCoroutine);
    }

    private IEnumerator TakeCurseDamage()
    {
        while (true)
        {   
            int currentDamage = (int)Mathf.Ceil(_curseController.Stacks * DefaultCurseDamage);

            if (_health > currentDamage)
            {
                _health -= currentDamage;
                _takenCurseDamage += currentDamage;
            }
            else
            {
                _health = 1;
            }

            yield return new WaitForSeconds(CurseDamagePeriod);
        }
    }
}
