using UnityEngine;

[RequireComponent(typeof(PlayerMovement), typeof(PlayerAttack))]
public class PlayerDeath : MonoBehaviour
{
    [SerializeField] private MainMenu _levelLoader;

    private PlayerMovement _movementScript;
    private PlayerAttack _attackScript;
    private Rigidbody2D _rigidBody;

    private void Start()
    {
        PlayerHealth.Death += Die;

        _movementScript = GetComponent<PlayerMovement>();
        _attackScript = GetComponent<PlayerAttack>();
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    private void Die()
    {
        AnimationHandler.Animator.Play(StatesAnimation.DeathName);

        _movementScript.enabled = false;
        _attackScript.enabled = false;
        _rigidBody.bodyType = RigidbodyType2D.Static;

        _levelLoader.LoadGame(1);
    }

}
