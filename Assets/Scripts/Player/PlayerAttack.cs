using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Transform _attackPoint;
    [SerializeField] private LayerMask _enemyLayers;
    [SerializeField] private AudioSource _audio;
        
    [Header("Stats")]
    [SerializeField] private int _damage = 3;
    [SerializeField] private float _range = 0.8f;
    [SerializeField] private float _attackRate = 2f;

    private PlayerStamina _staminaScript;

    public int Damage { 
        get => _damage; 
        private set => _damage = value; 
    }

    private float _nextAttackTime = 0f;

    private void Start()
    {
        InputHandler.Instance.AttackPressed += Hit;
        _staminaScript = GetComponent<PlayerStamina>();
    }

    private void Hit() 
    {   
        if (Time.time >= _nextAttackTime && _staminaScript.Stamina >= _staminaScript.RequiredValue)
        {
            AnimationHandler.Animator.SetTrigger(StatesAnimation.PressedAttack);
            
            _nextAttackTime = Time.time + 1f / _attackRate;
            
            _audio.Play();

            _staminaScript.ExpendStamina?.Invoke();
        }
    }

    private void HitEnemy()
    {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(_attackPoint.transform.position, _range, _enemyLayers);
        foreach (Collider2D enemy in enemies)
            enemy.gameObject.GetComponent<EnemyHealth>().Hit?.Invoke();
    }
}
