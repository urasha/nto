using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationHandler : MonoBehaviour
{
    public static Animator Animator { get; private set; }

    private void Start()
    {
        Animator = GetComponent<Animator>();
    }
}
