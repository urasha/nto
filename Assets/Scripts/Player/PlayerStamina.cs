using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PlayerStamina : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Image _staminaBar;

    [Header("Stats")]
    [SerializeField] private float _increaseValue;
    [SerializeField] private float _decreaseValue;

    public UnityAction ExpendStamina;

    public float RequiredValue { get => _decreaseValue; private set => _decreaseValue = value; }

    private readonly int _maxStamina = 100;

    private float _lerpSpeed = 3f;
    private float _currentStamina;
    public float Stamina { 
        get => _currentStamina; 
        private set => _currentStamina = value; 
    }

    private void Start()
    {
        _currentStamina = _maxStamina;
        RequiredValue = _decreaseValue;

        ExpendStamina += DecreaseStamina;
    }

    private void Update()
    {
        IncreaseStamina();
        FillStaminaBar();
    }

    private void FillStaminaBar()
    {
        _lerpSpeed = 3f * Time.deltaTime;
        _staminaBar.fillAmount = Mathf.Lerp(_staminaBar.fillAmount, (float)_currentStamina / (float)_maxStamina, _lerpSpeed);
    }

    private void IncreaseStamina()
    {
        _currentStamina = Mathf.Clamp(_increaseValue + _currentStamina, 0f, _maxStamina);
    }

    private void DecreaseStamina()
    {
        _currentStamina = Mathf.Clamp(_currentStamina - _decreaseValue, 0f, 100f);
    }
}
