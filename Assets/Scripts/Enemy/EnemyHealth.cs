using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class EnemyHealth : MonoBehaviour
{
    public UnityAction Death;
    public UnityAction Hit;

    [Header("General")]
    [SerializeField] private PlayerAttack _playerAttack;

    [Header("Stats")]
    [SerializeField] private int _health = 8;

    private Animator _animator;
    private EnemyDetection _detector;


    private void Start()
    {
        _animator = GetComponent<Animator>();
        _detector = GetComponent<EnemyDetection>();

        Hit += TakeDamage;
    }

    private void TakeDamage()
    {
        _animator.Play("Hit");
        _detector.Detected?.Invoke();
        _detector.IsDetected = true;

        _health -= _playerAttack.Damage;
        if (_health <= 0)
            Death?.Invoke();
    }
}
