using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnemyAttack : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private LayerMask _playerLayer;
    [SerializeField] private Animator _playerAnimator;
    [SerializeField] private PlayerHealth _playerHealthScript;

    [Header("Stats")]
    [SerializeField] private float _range;
    [SerializeField] private int _damage;
    [SerializeField] private float _attackRate;

    private Vector2 _originalDirection;
    private float _nextAttackTime = 0.15f;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, transform.localScale.x > 0 ? Vector2.left : Vector2.right, _range, _playerLayer);
        if (hits.Length > 0)
        {
            if (Time.time >= _nextAttackTime)
            {   
                _animator.Play("Attack");
                _nextAttackTime = Time.time + 1f / _attackRate;
                _originalDirection = transform.localScale.x > 0 ? Vector2.left : Vector2.right;
            }
        }
    }

    private void Attack()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, _originalDirection, _range, _playerLayer);
        if (hits.Length > 0 && !_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Roll"))
            _playerHealthScript.TakeDamage(_damage);
    }
}
