using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpriteRenderer))]
public class EnemyDetection : MonoBehaviour
{   
    [Header("General")]
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private LayerMask _playerLayer;

    [Header("Stats")]
    [SerializeField] private float _range;

    public UnityAction Detected;
    [HideInInspector] public bool IsDetected = false;

    private void Update()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, transform.localScale.x > 0 ? Vector2.left : Vector2.right, _range, _playerLayer);
        if (hits.Length > 0)
        {
            Detected?.Invoke();
            IsDetected = true;
        }
    }
}
