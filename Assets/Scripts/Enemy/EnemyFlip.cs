using UnityEngine;

[RequireComponent(typeof(EnemyDetection), typeof(Animator))]
public class EnemyFlip : MonoBehaviour
{   
    [SerializeField] private Transform _player;

    private readonly Vector2 _leftScale = new Vector2(-1f, 1f);
    private readonly Vector2 _rightScale = new Vector2(1f, 1f);
    
    private EnemyDetection _detector;
    private Animator _animator;

    private void Start()
    {
        _detector = GetComponent<EnemyDetection>();
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (_detector.IsDetected && _animator.GetCurrentAnimatorClipInfo(0)[0].clip.name != "Attack")
            Flip();
    }
    
    private void Flip()
    {
        if (_player.position.x < transform.position.x)
            transform.localScale = _rightScale;
        else
            transform.localScale = _leftScale;
    }
}