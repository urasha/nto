using UnityEngine;

[RequireComponent(typeof(EnemyHealth), typeof(Rigidbody2D), typeof(Collider2D))]
public class EnemyDeath : MonoBehaviour
{   
    private Animator _animator;
    private EnemyHealth _enemyHealth;
    private Collider2D _collider;
    private Rigidbody2D _rigidBody;
    private EnemyAttack _enemyAttack;
    private EnemyFlip _flipScript;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _flipScript = GetComponent<EnemyFlip>();
        _enemyHealth = GetComponent<EnemyHealth>();
        _collider = GetComponent<Collider2D>();
        _rigidBody = GetComponent<Rigidbody2D>();
        _enemyAttack = GetComponent<EnemyAttack>();

        _enemyHealth.Death += Die;
    }

    private void Die()
    {   
        _animator.Play("Death");

        _flipScript.enabled = false;
        _collider.enabled = false;
        _enemyAttack.enabled = false;
        _rigidBody.bodyType = RigidbodyType2D.Static;

        Destroy(gameObject, 0.6f);
    }

    private void DisableAnimator()
    {
        _animator.enabled = false;
    }
}