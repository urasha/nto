using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EnemyDetection), typeof(Animator), typeof(Rigidbody2D))]
public class EnemyMovement : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Transform _player;

    [Header("Stats")]
    [SerializeField] private float _speed;

    private EnemyDetection _detector;
    private Rigidbody2D _rigidBody;
    private Animator _animator;

    private void Start()
    {
        _detector = GetComponent<EnemyDetection>();
        _animator = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody2D>();

        _detector.Detected += StartMoving;
    }

    private void Update()
    {
        Debug.Log(_detector.IsDetected);
    }

    private void StartMoving()
    {
        StartCoroutine(Move());
    }

    private IEnumerator Move()
    {
        while (!_animator.GetCurrentAnimatorStateInfo(0).IsName("Death"))
        {   
            if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") && Vector3.Distance(_player.position, transform.position) > 1f)
            {
                _rigidBody.velocity = new Vector2(Mathf.Sign(_player.position.x - transform.position.x) * _speed, 0f);
            }
            else
            {
                _rigidBody.velocity = Vector2.zero;
            }

            yield return null;
        }
    }
}
